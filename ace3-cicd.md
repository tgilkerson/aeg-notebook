# ACT3 DevSecOps Pipelines

## CI/CD Overview

```mermaid
graph TB
  classDef projClass fill:#ff7700,stroke:#333;
  classDef repoClass fill:#e0e3de,stroke:#333;
  classDef pipelineClass fill:#00aeff,stroke:#333;

  %% 
  %% repos
  %%
  library-repo[(library repo)]
  image-repo[(image repo)] 
  chart-repo[(chart repo)]
  class library-repo,image-repo,chart-repo repoClass

  %% 
  %% projects
  %%
  library-proj(library project)
  charts-proj(charts project)
  env-proj(environment project)
  app-proj(application project)
  class library-proj,charts-proj,env-proj,app-proj projClass

  %% 
  %% pipelines
  %%  
  library-ci(CI pipeline)
  app-ci(CI pipeline)
  charts-ci(CI Pipeline)
  env-ci(CI Pipeline)
  class library-ci,app-ci,charts-ci,env-ci pipelineClass

  %% 
  %% library flow
  %%
  library-proj -->|git event| library-ci
  library-ci -->|publish| library-repo
  library-repo -->|dependency| app-proj

  %% 
  %% app flow
  %%
  app-proj -->|git event| app-ci
  app-ci -->|publish| image-repo
  app-ci -->|publish| chart-repo
  app-ci -->|gitops| env-proj

  %% 
  %% charts flow
  %%
  charts-proj -->|git event| charts-ci
  charts-ci -->|publish| chart-repo
  charts-ci -->|gitops| env-proj

  %% 
  %% env flow
  %%
  env-proj -->|git event| env-ci
  env-ci -->|deploy| K8S(kubernetes)
  ```

## Library Project CI/CD Sequence

```mermaid
sequenceDiagram
  participant Devs
  participant Library Project
  participant CI
  participant Library Repo

  Devs->>Library Project: Create MR
  
  Library Project->>CI: MR event
  activate CI
  CI-->>Library Project: pass/fail
  deactivate CI

  Devs->>Library Project: Review CI results of MR

  Devs->>Library Project: Merge to master
  Devs->>Library Project: SimVer tag master
  Library Project->>CI: Tag event

  activate CI
  CI->>Library Repo: Push module 
  CI-->>Library Project: pass/fail
  deactivate CI

  Devs->>Library Project: Review CI results of Tag


```

## Application Project CI/CD Sequence

```mermaid
sequenceDiagram
  participant Devs
  participant App Project
  participant CI
  participant Library Repo
  participant Container Repo
  participant Environment Project
  participant CD
  participant Chart Repo
  participant K8S
  
  Devs->>App Project: Create MR
  
  App Project->>CI: MR event
  activate CI
  CI-->>Library Repo: Pull libraries
  CI-->>App Project: pass/fail
  deactivate CI

  Devs->>App Project: Review CI results of MR

  Devs->>App Project: Merge to master
  Devs->>App Project: SimVer tag master
  App Project->>CI: Tag event

  activate CI
  CI-->>Library Repo: Pull libraries
  CI->>Container Repo: Push image
  CI->>Environment Project: Merge to master
  Note right of CI: GitOps - update app<br />version in env chart<br />and commit 
  CI-->>App Project: pass/fail
  deactivate CI

  Devs->>App Project: Review CI results of Tag

  Environment Project->>CD: Merge event
  activate CD
  CD->>K8S: Deploy
  K8S-->>Chart Repo: Pull charts
  CD-->>Environment Project: pass/fail
  deactivate CD

  Devs->>Environment Project: Review CD results
  K8S-->>Container Repo: Pull images

```

## Chart Project CI/CD Sequence

```mermaid
sequenceDiagram
  participant Devs
  participant Chart Project
  participant CI
  participant Library Repo
  participant Container Repo
  participant Environment Project
  participant CD
  participant Chart Repo
  participant K8S

  Devs->>Chart Project: Create MR

  Chart Project->>CI: MR event
  activate CI
  CI-->>Chart Project: pass/fail
  deactivate CI

  Devs->>Chart Project: Review CI results of MR
  Devs->>Chart Project: Merge to master
  Devs->>Chart Project: SimVer tag master

  Chart Project->>CI:Tag event
  activate CI
  CI->>Chart Repo:Push chart
  CI->>Environment Project: Merge to master
  Note right of CI: GitOps-update chart<br />version in env chart<br />and commit
  CI-->>Chart Project: pass/fail
  deactivate CI

  Devs->>Chart Project: Review CI results of Tag

  Environment Project->>CD: Merge event
  activate CD
  CD->>K8S: Deploy
  K8S-->>Chart Repo: Pull charts
  CD-->>Environment Project: pass/fail
  deactivate CD

  Devs->>Environment Project: Review CD results
  K8S-->>Container Repo: Pull images  
```
